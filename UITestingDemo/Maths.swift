//
//  Maths.swift
//  UITestingDemo
//
//  Created by James Cash on 23-02-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation

var cache: [Int:Int64] = [:]

struct Maths {
    static func slowFib(_ n: Int) -> Int64 {
        if n == 0 { return 1 }
        if n == 1 { return 1 }

        return fib(n-1) + fib(n-2)
    }

    // memoization
    static func fib(_ n: Int) -> Int64 {
        if n == 0 { return 1 }
        if n == 1 { return 1 }
        if let res = cache[n] {
            return res
        } else {
            let res =  fib(n-1) + fib(n-2)
            cache[n] = res
            return res
        }
    }
}
