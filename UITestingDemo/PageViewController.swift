//
//  PageViewController.swift
//  UITestingDemo
//
//  Created by James Cash on 23-02-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {

    var pages = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()

        guard let page1 = storyboard?.instantiateViewController(withIdentifier: "pageOne"),
            let page2 = storyboard?.instantiateViewController(withIdentifier: "pageTwo"),
            let page3 = storyboard?.instantiateViewController(withIdentifier: "pageThree") else {
                abort()
        }

        pages += [page1, page2, page3]

        setViewControllers([page1], direction: .forward, animated: false, completion: nil)

        dataSource = self

        // Do any additional setup after loading the view.
    }
}

extension PageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let idx = pages.index(of: viewController),
            (idx + 1) < pages.count else { return nil }
        return pages[idx + 1]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let idx = pages.index(of: viewController),
            idx > 0 else { return nil }
        return pages[idx - 1]

    }
}
